package main

import (
	"flag"
	"log"
	"os"
	"strings"
)

func defaultsFromEnv(fs *flag.FlagSet) {
	fs.VisitAll(func(f *flag.Flag) {
		envName := "ENQ_" + strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
		if s := os.Getenv(envName); s != "" {
			if err := f.Value.Set(s); err != nil {
				log.Printf("error setting flag %s from env variable %s: %v", f.Name, envName, err)
				return
			}
			f.DefValue = s
		}
	})
}
