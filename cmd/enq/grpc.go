package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"git.autistici.org/ai3/tools/enq/engine"
)

// Handle shared-token authentication from clients.
func authTokenInterceptor(token string) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Errorf(codes.InvalidArgument, "Failed to retrieve request metadata")
		}

		authHeader, ok := md["authorization"]
		if !ok {
			return nil, status.Errorf(codes.Unauthenticated, "Missing authorization token")
		}
		if authHeader[0] != token {
			return nil, status.Errorf(codes.Unauthenticated, "Authentication failed")
		}

		return handler(ctx, req)
	}
}

// Auxiliary type to set per-request RPC credentials with a shared
// authentication token.
type grpcAuth struct {
	meta map[string]string
}

func newGRPCAuth(authToken string) *grpcAuth {
	return &grpcAuth{
		meta: map[string]string{"authorization": authToken},
	}
}

func (g *grpcAuth) GetRequestMetadata(_ context.Context, _ ...string) (map[string]string, error) {
	return g.meta, nil
}

func (g *grpcAuth) RequireTransportSecurity() bool {
	return true
}

func newGRPCDialer(authToken, sslCert, sslKey, sslCA string) engine.Dialer {
	return func(ctx context.Context, addr string) (*grpc.ClientConn, error) {
		var opts []grpc.DialOption

		if sslCert != "" && sslKey != "" && sslCA != "" {
			tlsconf, err := clientTLSConfig(sslCert, sslKey, sslCA)
			if err != nil {
				return nil, err
			}
			opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(tlsconf)))
		} else {
			opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
		}

		if authToken != "" {
			opts = append(opts, grpc.WithPerRPCCredentials(newGRPCAuth(authToken)))
		}

		return grpc.DialContext(ctx, addr, opts...)
	}
}

func clientTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
		RootCAs:      ca,
	}, nil
}

func serverTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates:             []tls.Certificate{cert},
		MinVersion:               tls.VersionTLS12,
		PreferServerCipherSuites: true,
		ClientAuth:               tls.RequireAndVerifyClientCert,
		ClientCAs:                ca,
	}, nil
}

func loadCA(path string) (*x509.CertPool, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	if !cas.AppendCertsFromPEM(data) {
		return nil, fmt.Errorf("no certificates could be parsed in %s", path)
	}
	return cas, nil
}
