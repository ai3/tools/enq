package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/google/subcommands"
	"golang.org/x/sync/errgroup"
)

func tryAFewTimes(f func() error) func() error {
	return func() (err error) {
		for i := 0; i < 5; i++ {
			err = f()
			if err == nil {
				break
			}
			time.Sleep(1 * time.Second)
		}
		return
	}
}

func TestServer(t *testing.T) {
	tmpdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(tmpdir)

	// Create a handler that will just dump its output to a file
	// named 'output'.
	outfile := tmpdir + "/output"
	os.Mkdir(tmpdir+"/handlers", 0755)
	ioutil.WriteFile(tmpdir+"/handlers/test-method", []byte(
		fmt.Sprintf("#!/bin/sh\ncat >> %s\n", outfile)), 0755)

	// Outer context will have a global timeout.
	outerCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	g, ctx := errgroup.WithContext(outerCtx)

	// Run a server.
	g.Go(func() error {
		serverCmd := &serverCommand{
			shard:    "1",
			addr:     ":3733",
			dbPath:   tmpdir + "/sql.db",
			topology: "static:1=127.0.0.1:3733",
		}

		exitStatus := serverCmd.Execute(ctx, flag.NewFlagSet("", flag.ContinueOnError))
		if exitStatus != subcommands.ExitSuccess {
			return fmt.Errorf("server exited with status %v", exitStatus)
		}
		return nil
	})

	// Run a worker.
	g.Go(func() error {
		workerCmd := &workerCommand{
			serverAddr: "127.0.0.1:3733",
			scriptDir:  tmpdir + "/handlers",
			id:         "foo",
			numWorkers: 1,
		}

		exitStatus := workerCmd.Execute(ctx, flag.NewFlagSet("", flag.ContinueOnError))
		if exitStatus != subcommands.ExitSuccess {
			return fmt.Errorf("worker exited with status %v", exitStatus)
		}
		return nil
	})

	// Submit a task using the command line.
	g.Go(tryAFewTimes(func() error {
		submitCmd := &submitCommand{
			serverAddr: "127.0.0.1:3733",
		}

		taskData := `[{
    "task_id": {
        "shard": "1",
        "id": "mytask"
    },
    "method": "test-method",
    "args": [
        {
            "type": "VALUE",
            "value_json": "eyJ1aWQiOiAxMjM0fQ=="
        }
    ]
}]`

		if err := submitCmd.submit(ctx, strings.NewReader(taskData)); err != nil {
			return fmt.Errorf("submission error: %w", err)
		}
		log.Printf("task submitted")
		return nil
	}))

	// This is a way to kill the stack early once the output is generated.
	var errAllDone = errors.New("all done")
	g.Go(func() error {
		tick := time.NewTicker(100 * time.Millisecond)
		defer tick.Stop()
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-tick.C:
				if _, err := os.Stat(outfile); err == nil {
					return errAllDone
				}
			}
		}
	})

	err = g.Wait()
	switch err {
	case errAllDone:
	case nil:
		// Should not happen (in case of a timeout waiting for
		// the output we would get a deadline exceeded error).
		t.Error("all commands exited successfully?")
	default:
		t.Error(err)
	}

	if _, err := os.Stat(outfile); err != nil {
		t.Errorf("output file does not exist: %v", err)
	}
}
