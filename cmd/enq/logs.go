package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/google/subcommands"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

type logsCommand struct {
	serverAddr string
	authToken  string
	sslCert    string
	sslKey     string
	sslCA      string
}

func (c *logsCommand) Name() string     { return "logs" }
func (c *logsCommand) Synopsis() string { return "retrieve task logs" }
func (c *logsCommand) Usage() string {
	return `logs [<options>] <task_id>:
        Query the engine to retrieve task logs.

`
}

func (c *logsCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "127.0.0.1:3733", "address of the local queue engine server")
	f.StringVar(&c.authToken, "auth-token", "", "shared authentication secret")
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
	defaultsFromEnv(f)
}

func (c *logsCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("error: wrong number of arguments")
		return subcommands.ExitUsageError
	}

	tid, err := pb.ParseShardedID(f.Arg(0))
	if err != nil {
		log.Printf("error: parsing task ID: %v", err)
		return subcommands.ExitUsageError
	}

	if err := c.logs(ctx, tid); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *logsCommand) logs(ctx context.Context, tid *pb.ShardedID) error {
	// Initialize the GRPC connection.
	dialer := newGRPCDialer(c.authToken, c.sslCert, c.sslKey, c.sslCA)
	conn, err := dialer(ctx, c.serverAddr)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewQueueClient(conn)

	stream, err := client.GetLogs(ctx, &pb.GetLogsRequest{TaskId: tid})
	if err != nil {
		return fmt.Errorf("remote error: %w", err)
	}
	for {
		chunk, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		os.Stdout.Write(chunk.Data)
	}
	return nil
}

func init() {
	subcommands.Register(&logsCommand{}, "")
}
