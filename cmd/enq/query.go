package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"github.com/google/subcommands"
	"google.golang.org/protobuf/encoding/protojson"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

type queryCommand struct {
	serverAddr string
	authToken  string
	sslCert    string
	sslKey     string
	sslCA      string
}

func (c *queryCommand) Name() string     { return "query" }
func (c *queryCommand) Synopsis() string { return "retrieve the status of a task" }
func (c *queryCommand) Usage() string {
	return `query [<options>] <task_id>:
        Query the engine to retrieve the status of a task.

`
}

func (c *queryCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "127.0.0.1:3733", "address of the local queue engine server")
	f.StringVar(&c.authToken, "auth-token", "", "shared authentication secret")
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
	defaultsFromEnv(f)
}

func (c *queryCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("error: wrong number of arguments")
		return subcommands.ExitUsageError
	}

	tid, err := pb.ParseShardedID(f.Arg(0))
	if err != nil {
		log.Printf("error: parsing task ID: %v", err)
		return subcommands.ExitUsageError
	}

	if err := c.query(ctx, tid); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *queryCommand) query(ctx context.Context, tid *pb.ShardedID) error {
	// Initialize the GRPC connection.
	dialer := newGRPCDialer(c.authToken, c.sslCert, c.sslKey, c.sslCA)
	conn, err := dialer(ctx, c.serverAddr)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := pb.NewQueueClient(conn)

	resp, err := client.Query(ctx, &pb.QueryRequest{TaskId: tid})
	if err != nil {
		return fmt.Errorf("remote error: %w", err)
	}

	marshaler := protojson.MarshalOptions{
		Multiline:     true,
		UseProtoNames: true,
	}
	data, err := marshaler.Marshal(resp)
	if err != nil {
		return fmt.Errorf("error encoding response: %w", err)
	}
	fmt.Printf("%s\n", data)
	return nil
}

func init() {
	subcommands.Register(&queryCommand{}, "")
}
