package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/google/subcommands"
	"google.golang.org/protobuf/encoding/protojson"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

type submitCommand struct {
	serverAddr string
	authToken  string
	sslCert    string
	sslKey     string
	sslCA      string
}

func (c *submitCommand) Name() string     { return "submit" }
func (c *submitCommand) Synopsis() string { return "submit a task tree" }
func (c *submitCommand) Usage() string {
	return `submit:
        Submit one or more task to the engine. The command expects
        a list of JSON-encoded TaskSpec protos on standard input.

`
}

func (c *submitCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "127.0.0.1:3733", "address of the local queue engine server")
	f.StringVar(&c.authToken, "auth-token", "", "shared authentication secret")
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
	defaultsFromEnv(f)
}

func (c *submitCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	if err := c.submit(ctx, os.Stdin); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *submitCommand) submit(ctx context.Context, r io.Reader) error {
	// Initialize the GRPC connection.
	dialer := newGRPCDialer(c.authToken, c.sslCert, c.sslKey, c.sslCA)
	conn, err := dialer(ctx, c.serverAddr)
	if err != nil {
		return err
	}
	defer conn.Close()

	// Read tasks from standard input. To simplify the input,
	// we wrap a SubmitRequest JSON around the list of objects
	// taken from stdin, so we can call protojson.Unmarshal
	// on the fully-formed proto message.
	var buf bytes.Buffer
	io.WriteString(&buf, "{\"tasks\":") // nolint: errcheck
	if _, err := io.Copy(&buf, r); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}
	io.WriteString(&buf, "}") // nolint: errcheck

	var req pb.SubmitRequest
	if err := protojson.Unmarshal(buf.Bytes(), &req); err != nil {
		return fmt.Errorf("reading input: %w", err)
	}

	client := pb.NewQueueClient(conn)
	resp, err := client.Submit(ctx, &req)
	if err != nil {
		return fmt.Errorf("remote error: %w", err)
	}

	fmt.Printf("%s\n", resp.RootTaskId.ToString())
	return nil
}

func init() {
	subcommands.Register(&submitCommand{}, "")
}
