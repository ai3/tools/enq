package engine

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"git.autistici.org/ai3/tools/enq/migrations"
	pb "git.autistici.org/ai3/tools/enq/proto"
	migrate "github.com/golang-migrate/migrate/v4"
	msqlite3 "github.com/golang-migrate/migrate/v4/database/sqlite3"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	sqlite3 "github.com/mattn/go-sqlite3"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var debugMigrations = true

// OpenDB opens a SQLite database and runs the database migrations.
func OpenDB(dburi string) (*sql.DB, error) {
	// Add some sqlite3-specific parameters if none are specified.
	if !strings.Contains(dburi, "?") {
		dburi += "?cache=shared&_busy_timeout=10000&_journal=WAL"
	}

	db, err := sql.Open("sqlite3", dburi)
	if err != nil {
		return nil, err
	}

	if err = runDatabaseMigrations(db); err != nil {
		db.Close() // nolint
		return nil, err
	}

	return db, nil
}

type migrateLogger struct{}

func (l migrateLogger) Printf(format string, v ...interface{}) {
	log.Printf("db: "+format, v...)
}

func (l migrateLogger) Verbose() bool { return true }

func runDatabaseMigrations(db *sql.DB) error {
	si, err := bindata.WithInstance(bindata.Resource(
		migrations.AssetNames(),
		migrations.Asset,
	))
	if err != nil {
		return err
	}

	di, err := msqlite3.WithInstance(db, &msqlite3.Config{
		MigrationsTable: msqlite3.DefaultMigrationsTable,
		DatabaseName:    "enq",
	})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("bindata", si, "sqlite3", di)
	if err != nil {
		return err
	}

	if debugMigrations {
		m.Log = &migrateLogger{}
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

type sqldb struct {
	*sql.DB
	shard string
}

func newSQLDB(shard string, db *sql.DB) *sqldb {
	return &sqldb{
		DB:    db,
		shard: shard,
	}
}

func (d *sqldb) WithTx(ctx context.Context, f func(databaseTx) error) error {
	tx, err := d.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	if err := f(&sqltx{Tx: tx, shard: d.shard}); err != nil {
		tx.Rollback() // nolint
		return err
	}

	return tx.Commit()
}

type sqltx struct {
	*sql.Tx
	shard string
}

func wrapErr(err error) error {
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return status.Error(codes.NotFound, "not found")
	}
	return err
}

func (x *sqltx) assertLocalShard(id *pb.ShardedID) {
	if id.Shard != x.shard {
		panic(fmt.Sprintf("non-local data (shard %s) accessed on shard %s", id.Shard, x.shard))
	}
}

func (x *sqltx) GetTaskSpec(id *pb.ShardedID) (*pb.TaskSpec, error) {
	x.assertLocalShard(id)

	var spec pb.TaskSpec
	var notBefore time.Time
	var dsid string
	if err := x.Tx.QueryRow("SELECT method, not_before, downstream_task_id FROM tasks WHERE id = ?", id.ToString()).Scan(&spec.Method, &notBefore, &dsid); err != nil {
		return nil, wrapErr(err)
	}
	spec.NotBefore = timestamppb.New(notBefore)
	spec.TaskId = id
	if dsid != "" {
		parsed, err := pb.ParseShardedID(dsid)
		if err != nil {
			return nil, fmt.Errorf("bad downstream_task_id: %w", err)
		}
		spec.DownstreamTaskId = parsed
	}

	rows, err := x.Tx.Query("SELECT arg_type, value_json, ref_task_id FROM task_args WHERE task_id = ?", id.ToString())
	if err != nil {
		return nil, wrapErr(err)
	}
	defer rows.Close()
	for rows.Next() {
		var arg pb.ArgumentSpec
		var refID string
		if err := rows.Scan(&arg.Type, &arg.ValueJson, &refID); err != nil {
			return nil, err
		}
		if arg.Type == pb.ArgumentSpec_TASKREF {
			arg.TaskId, err = pb.ParseShardedID(refID)
			if err != nil {
				return nil, fmt.Errorf("error parsing taskref: %w", err)
			}
		}
		spec.Args = append(spec.Args, &arg)
	}
	return &spec, rows.Err()
}

func (x *sqltx) GetTaskCreatedAt(id *pb.ShardedID) (t time.Time, err error) {
	err = x.Tx.QueryRow("SELECT created_at FROM tasks WHERE id = ?", id.ToString()).Scan(&t)
	return
}

func (x *sqltx) GetTaskState(id *pb.ShardedID) (pb.TaskInfo_State, error) {
	x.assertLocalShard(id)

	idStr := id.ToString()
	var state pb.TaskInfo_State
	err := x.Tx.QueryRow(fmt.Sprintf(
		"SELECT %d FROM scheduled WHERE task_id = ? UNION SELECT %d FROM running WHERE task_id = ? UNION SELECT %d FROM pending WHERE task_id = ? UNION SELECT state FROM done WHERE task_id = ?",
		pb.TaskInfo_SCHEDULABLE,
		pb.TaskInfo_RUNNING,
		pb.TaskInfo_PENDING,
	), idStr, idStr, idStr, idStr).Scan(&state)
	return state, wrapErr(err)
}

func (x *sqltx) IsSchedulable(id *pb.ShardedID) bool {
	var count int
	// Do we have all results for all tasks referenced as
	// arguments of this task? This can only inspect local
	// information (the results table).
	if err := x.Tx.QueryRow("SELECT COUNT(*) FROM task_args LEFT JOIN results ON task_args.ref_task_id = results.task_id WHERE task_args.task_id = ? AND task_args.arg_type = ? AND results.value IS NULL", id.ToString(), pb.ArgumentSpec_TASKREF).Scan(&count); err != nil {
		return false
	}
	return count == 0
}

func (x *sqltx) AddTask(spec *pb.TaskSpec) error {
	x.assertLocalShard(spec.TaskId)

	if _, err := x.Tx.Exec("INSERT INTO tasks (id, method, not_before, downstream_task_id, created_at) VALUES (?, ?, ?, ?, ?)", spec.TaskId.ToString(), spec.Method, spec.NotBefore.AsTime(), spec.DownstreamTaskId.ToString(), time.Now()); err != nil {
		return fmt.Errorf("AddTask(tasks): %w", err)
	}
	for _, arg := range spec.Args {
		if _, err := x.Tx.Exec("INSERT INTO task_args (task_id, arg_type, value_json, ref_task_id) VALUES (?, ?, ?, ?)", spec.TaskId.ToString(), arg.Type, arg.ValueJson, arg.TaskId.ToString()); err != nil {
			return fmt.Errorf("AddTasks(task_args): %w", err)
		}
	}

	if x.IsSchedulable(spec.TaskId) {
		return x.saveToSchedulable(spec.TaskId, spec.NotBefore.AsTime())
	}
	return x.saveToPending(spec.TaskId)
}

// SetTaskState only modifies the task state and the related queue
// mechanisms, it does not modify any other task parameters or verify
// invariants.
func (x *sqltx) SetTaskState(id *pb.ShardedID, state pb.TaskInfo_State, when time.Time) error {
	x.assertLocalShard(id)

	// Clear task from all state tables.
	for _, tbl := range []string{"scheduled", "pending", "running", "done"} {
		if _, err := x.Tx.Exec(fmt.Sprintf("DELETE FROM %s WHERE task_id = ?", tbl), id.ToString()); err != nil {
			return err
		}
	}

	var err error
	switch state {
	case pb.TaskInfo_PENDING:
		err = x.saveToPending(id)
	case pb.TaskInfo_SCHEDULABLE:
		err = x.saveToSchedulable(id, when)
	case pb.TaskInfo_RUNNING:
		// This transition is exclusively handled by NextLease.
		err = errors.New("unexpected SetTaskState(RUNNING)")
	default:
		err = x.saveToDone(id, state)
	}
	return err
}

func (x *sqltx) saveToPending(id *pb.ShardedID) error {
	log.Printf("db %s: task %s state is PENDING", x.shard, id.ToString())
	_, err := x.Tx.Exec("INSERT INTO pending (task_id) VALUES (?)", id.ToString())
	return err
}

func (x *sqltx) saveToSchedulable(id *pb.ShardedID, when time.Time) error {
	log.Printf("db %s: task %s state is SCHEDULABLE", x.shard, id.ToString())
	_, err := x.Tx.Exec("INSERT INTO scheduled (task_id, schedule_time) VALUES (?, ?)", id.ToString(), when)
	return err
}

func (x *sqltx) saveToRunning(id *pb.ShardedID, uri string) error {
	log.Printf("db %s: task %s state is RUNNING", x.shard, id.ToString())
	_, err := x.Tx.Exec("INSERT INTO running (task_id, started_at, url) VALUES (?, ?, ?)", id.ToString(), time.Now(), uri)
	return err
}

func (x *sqltx) saveToDone(id *pb.ShardedID, state pb.TaskInfo_State) error {
	log.Printf("db %s: task %s state is %s", x.shard, id.ToString(), state)
	_, err := x.Tx.Exec("INSERT INTO done (task_id, state, completed_at) VALUES (?, ?, ?)", id.ToString(), state, time.Now())
	return err
}

// SetTaskResultID links a task with its result. Only used for debugging purposes.
func (x *sqltx) SetTaskResultID(taskID, resultID *pb.ShardedID) error {
	_, err := x.Tx.Exec("UPDATE tasks SET result_id = ? WHERE id = ?", resultID.ToString(), taskID.ToString())
	return err
}

func (x *sqltx) GetTaskResultByID(resultID *pb.ShardedID) ([]byte, error) {
	var result []byte
	err := x.Tx.QueryRow("SELECT value FROM results WHERE result_id = ?", resultID.ToString()).Scan(&result)
	return result, wrapErr(err)
}

func (x *sqltx) GetTaskResultByTaskID(id *pb.ShardedID) ([]byte, error) {
	var result []byte
	err := x.Tx.QueryRow("SELECT value FROM results WHERE task_id = ?", id.ToString()).Scan(&result)
	return result, wrapErr(err)
}

func (x *sqltx) StoreTaskResult(taskID *pb.ShardedID, value []byte) (*pb.ShardedID, error) {
	resultID := newUniqueID(x.shard)

	log.Printf("db %s: storing result %s of task %s", x.shard, resultID.ToString(), taskID.ToString())

	_, err := x.Tx.Exec("INSERT INTO results (result_id, task_id, value) VALUES (?, ?, ?)", resultID.ToString(), taskID.ToString(), value)
	return resultID, err
}

func (x *sqltx) PushTaskStateTransition(tst *pb.TaskStateTransition) (rowID, error) {
	result, err := x.Tx.Exec("INSERT INTO pending_transitions (task_id, result_status, result_data, pending_since) VALUES (?, ?, ?, ?)", tst.TaskId.ToString(), tst.ResultStatus, tst.ResultData, time.Now())
	if err != nil {
		return 0, err
	}
	tid, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return rowID(tid), nil
}

func (x *sqltx) PopTaskStateTransition(tid rowID) (*pb.TaskStateTransition, error) {
	var tst pb.TaskStateTransition
	var taskID string
	if err := x.Tx.QueryRow("SELECT task_id, result_status, result_data FROM pending_transitions WHERE id = ?", tid).Scan(&taskID, &tst.ResultStatus, &tst.ResultData); err != nil {
		return nil, err
	}
	id, err := pb.ParseShardedID(taskID)
	if err != nil {
		return nil, err
	}
	tst.TaskId = id
	_, err = x.Tx.Exec("DELETE FROM pending_transitions WHERE id = ?", tid)
	return &tst, err
}

func (x *sqltx) DeleteTaskStateTransition(tid rowID) error {
	_, err := x.Tx.Exec("DELETE FROM pending_transitions WHERE id = ?", tid)
	return err
}

func (x *sqltx) OnPendingTaskStateTransitions(f func(rowID) error) error {
	rows, err := x.Tx.Query("SELECT id FROM pending_transitions")
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var tid rowID
		if err := rows.Scan(&tid); err != nil {
			continue
		}
		if err := f(tid); err != nil {
			return err
		}
	}
	return rows.Err()
}

func (x *sqltx) NextTaskStateTransition() (*pb.TaskStateTransition, error) {
	var tst pb.TaskStateTransition
	var taskID string
	var rowID int64
	err := x.Tx.QueryRow("SELECT id, task_id, result_status, result_data FROM pending_transitions ORDER BY id ASC LIMIT 1").Scan(&rowID, &taskID, &tst.ResultStatus, &tst.ResultData)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	tst.TaskId, err = pb.ParseShardedID(taskID)
	if err != nil {
		return nil, err
	}
	_, err = x.Tx.Exec("DELETE FROM pending_transitions WHERE id = ?", rowID)
	return &tst, err
}

func (x *sqltx) NextLease(workerID, workerURL string) (*pb.ShardedID, error) {
	// We need to discard malformed entries, or we're going to be
	// stuck with them forever. The loop is meant to skip over
	// un-parsable IDs.
	for {
		var taskID string
		if err := x.Tx.QueryRow("SELECT task_id FROM scheduled WHERE schedule_time < ? ORDER BY schedule_time ASC LIMIT 1", time.Now()).Scan(&taskID); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}
			return nil, fmt.Errorf("NextLease(QueryRow): %w", err)
		}
		if _, err := x.Tx.Exec("DELETE FROM scheduled WHERE task_id = ?", taskID); err != nil {
			return nil, fmt.Errorf("NextLease(Delete): %w", err)
		}

		id, err := pb.ParseShardedID(taskID)
		if err != nil {
			log.Printf("skipping malformed task in scheduled queue: %v", err)
			continue
		}

		uri := fmt.Sprintf("%s/debug/tasks/%s", workerURL, taskID)
		err = x.saveToRunning(id, uri)
		return id, err
	}
}

func (x *sqltx) RenewLease(id *pb.ShardedID) error {
	_, err := x.Tx.Exec("UPDATE running SET pinged_at = ? WHERE task_id = ?", time.Now(), id.ToString())
	return err
}

// GetExpiredLeases returns a list of task IDs whose leases have expired.
func (x *sqltx) GetExpiredLeases() ([]*pb.ShardedID, error) {
	cutoff := time.Now().Add(-leaseTimeout)
	rows, err := x.Tx.Query("SELECT task_id FROM running WHERE pinged_at < ?", cutoff)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var expired []*pb.ShardedID
	for rows.Next() {
		var taskID string
		if err := rows.Scan(&taskID); err != nil {
			// Ignore this error, keep scanning.
			continue
		}
		id, err := pb.ParseShardedID(taskID)
		if err != nil {
			// Ignore this error, keep scanning.
			continue
		}
		expired = append(expired, id)
	}
	return expired, rows.Err()
}

func (x *sqltx) AddLog(id *pb.ShardedID, data []byte) error {
	_, err := x.Tx.Exec("INSERT INTO logs (task_id, data, timestamp) VALUES (?, ?, ?)", id.ToString(), data, time.Now())
	return err
}

func (x *sqltx) GetLogs(id *pb.ShardedID) (io.ReadCloser, error) {
	// We don't check the final rows.Err() as we have no way to
	// return an error to the caller.
	// nolint: rowserrcheck
	rows, err := x.Tx.Query("SELECT data FROM logs WHERE task_id = ? ORDER BY timestamp ASC", id.ToString())
	if err != nil {
		return nil, err
	}

	r, w := io.Pipe()
	go func() {
		defer rows.Close()
		defer w.Close()
		for rows.Next() {
			var data []byte
			if err := rows.Scan(&data); err != nil {
				return
			}
			if _, err := w.Write(data); err != nil {
				return
			}
		}
	}()

	return r, nil
}

func (x *sqltx) GetPendingTasks(limit int) ([]*pb.ShardedID, error) {
	rows, err := x.Tx.Query("SELECT task_id FROM pending UNION ALL SELECT task_id FROM scheduled ORDER BY task_id ASC LIMIT ?", limit)
	if err != nil {
		return nil, wrapErr(err)
	}
	defer rows.Close()

	var out []*pb.ShardedID
	for rows.Next() {
		var taskID string
		if err := rows.Scan(&taskID); err != nil {
			continue
		}
		id, err := pb.ParseShardedID(taskID)
		if err != nil {
			continue
		}
		out = append(out, id)
	}
	return out, rows.Err()
}

func (x *sqltx) GetStats() (*stats, error) {
	var s stats
	for _, p := range []struct {
		table string
		ptr   *int
	}{
		{"running", &s.NumRunning},
		{"scheduled", &s.NumSchedulable},
		{"pending", &s.NumPending},
		{"done", &s.NumDone},
	} {
		if err := x.Tx.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM %s", p.table)).Scan(p.ptr); err != nil {
			return nil, err
		}
	}
	return &s, nil
}

func (x *sqltx) tasksToDelete(maxAge time.Duration, limit int) ([]string, error) {
	deadline := time.Now().Add(-maxAge)
	rows, err := x.Tx.Query("SELECT task_id FROM done WHERE completed_at < ? ORDER BY task_id ASC LIMIT ?", deadline, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var toDelete []string
	for rows.Next() {
		var id string
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		toDelete = append(toDelete, id)
	}
	return toDelete, nil
}

var taskDeleteStmts = []string{
	"DELETE FROM results WHERE task_id IN (%s)",
	"DELETE FROM logs WHERE task_id IN (%s)",
	"DELETE FROM done WHERE task_id IN (%s)",
	"DELETE FROM task_args WHERE task_id IN (%s)",
	"DELETE FROM execution_log WHERE task_id IN (%s)",
	"DELETE FROM tasks WHERE task_id IN (%s)",
}

func (x *sqltx) DeleteOldTasks(maxAge time.Duration, limit int) (int, error) {
	toDelete, err := x.tasksToDelete(maxAge, limit)
	if err != nil {
		return 0, err
	}

	// Build IN string with the task IDs.
	inStr := strings.Join(toDelete, ",")

	for _, stmtTpl := range taskDeleteStmts {
		stmt := fmt.Sprintf(stmtTpl, inStr)
		if _, err := x.Tx.Exec(stmt); err != nil {
			return 0, err
		}
	}

	return len(toDelete), nil
}

// Detect a temporary (retriable at the GRPC layer) SQLite error.
func isBusy(err error) bool {
	var e sqlite3.Error
	if errors.As(err, &e) {
		switch e.Code {
		case sqlite3.ErrBusy, sqlite3.ErrLocked, sqlite3.ErrIoErr:
			return true
		}
	}
	return false
}

// SqliteServerInterceptor is a GRPC interceptor that translates
// low-level sqlite locking errors to meaningful GRPC error codes.
func SqliteServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		resp, err := handler(ctx, req)
		if err != nil && isBusy(err) {
			err = status.Error(codes.Unavailable, err.Error())
		}
		return resp, err
	}
}
