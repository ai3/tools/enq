package engine

import (
	"bytes"
	"context"
	"encoding/base64"
	"html/template"
	"io"
	"log"
	"net/http"
	"os/exec"
	"strconv"
	ttemplate "text/template"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

var (
	taskListSrc = `<!doctype html>
<html lang="en">
 <head>
  <title>ENQ</title>
 </head>
 <body>

  <h1>ENQ :: Overview</h1>

  <h3>Statistics</h3>
  <p>
    Running: {{.Stats.NumRunning}}<br>
    Schedulable: {{.Stats.NumSchedulable}}<br>
    Pending: {{.Stats.NumPending}}<br>
    Done: {{.Stats.NumDone}}
  </p>

  <h3>Pending tasks</h3>
  <table>
   <tbody>
{{range .PendingTasks}}
    <tr>
     <td>
      <a href="/debug/task?t={{.Spec.TaskId.ToString}}">{{.Spec.TaskId.ToString}}</a>
     </td>
     <td>{{.Spec.Method}}</td>
     <td>{{.State}}</td>
    </tr>
{{end}}
  </tbody>
  </table>

 </body>
</html>`

	taskDebugSrc = `<!doctype html>
<html lang="en">
 <head>
  <title>ENQ</title>
 </head>
 <body>

  <h1>ENQ :: Task {{.Spec.TaskId.ToString}}</h1>

  <p>
    Method: <b>{{.Spec.Method}}</b><br>
    State: <b>{{.State}}</b>
    Args:
{{range $idx, $arg := .Spec.Args}}
{{if gt $idx 0}}, {{end}}
{{if eq $arg.Type 0}}{{$arg.ValueJson}}{{else}}
<a href="/debug/task?t={{$arg.TaskId.ToString}}">{{$arg.TaskId.ToString}}</a>{{end}}
{{end}}
  </p>

  <h3>Tree</h3>
  <img src="data:image/svg+xml;base64,{{.EncodedSVG}}">

 </body>
</html>`

	dotTplSrc = `digraph task_tree {
  {
    rank=max
    "{{.Root.Spec.TaskId.ToString}}"
  }
{{range .Edges}}
  "{{.From.ToString}}" -> "{{.To.ToString}}"{{end}}
}`

	debugTpl *template.Template
	dotTpl   *ttemplate.Template
)

func init() {
	debugTpl = template.New("")
	template.Must(debugTpl.New("list").Parse(taskListSrc))
	template.Must(debugTpl.New("task").Parse(taskDebugSrc))

	dotTpl = ttemplate.New("")
	ttemplate.Must(dotTpl.New("dot").Parse(dotTplSrc))
}

func renderDebugTemplate(w http.ResponseWriter, name string, vars interface{}) {
	w.Header().Set("Cache-Control", "no-cache, no-store")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	var buf bytes.Buffer
	if err := debugTpl.Lookup(name).Execute(&buf, vars); err != nil {
		log.Printf("error rendering debug page: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf) // nolint: errcheck
}

func (s *Engine) handleTaskDebugPage(w http.ResponseWriter, req *http.Request) {
	taskID, err := pb.ParseShardedID(req.FormValue("t"))
	if err != nil {
		http.Error(w, "bad task ID", http.StatusBadRequest)
		return
	}

	tree := getTaskGraph(req.Context(), s.net, taskID)
	if len(tree) == 0 {
		http.NotFound(w, req)
		return
	}

	svg, err := makeEncodedSVG(tree)
	if err != nil {
		log.Printf("error generating SVG: %v", err)
	}

	renderDebugTemplate(w, "task", struct {
		Spec       *pb.TaskSpec
		State      pb.TaskInfo_State
		EncodedSVG string
	}{tree[0].Spec, tree[0].State, svg})
}

func (s *Engine) handleTaskListPage(w http.ResponseWriter, req *http.Request) {
	limit, _ := strconv.Atoi(req.FormValue("n"))

	var tasks []*pb.TaskInfo
	var taskStats *stats
	err := s.db.WithTx(req.Context(), func(tx databaseTx) error {
		pending, err := tx.GetPendingTasks(limit)
		if err != nil {
			return err
		}
		for _, id := range pending {
			spec, err := tx.GetTaskSpec(id)
			if err != nil {
				continue
			}
			state, err := tx.GetTaskState(id)
			if err != nil {
				continue
			}
			tasks = append(tasks, &pb.TaskInfo{
				Spec:  spec,
				State: state,
			})
		}

		taskStats, err = tx.GetStats()
		return err
	})
	if err != nil {
		log.Printf("debug page error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	renderDebugTemplate(w, "list", struct {
		PendingTasks []*pb.TaskInfo
		Stats        *stats
	}{tasks, taskStats})
}

type edge struct {
	From, To *pb.ShardedID
}

func graphEdges(tasks []*pb.TaskInfo) []edge {
	var edges []edge
	for _, task := range tasks {
		for _, tid := range task.Spec.GetTaskArgIDs() {
			edges = append(edges, edge{
				From: tid,
				To:   task.Spec.TaskId,
			})
		}
	}
	return edges
}

func makeEncodedSVG(tasks []*pb.TaskInfo) (string, error) {
	var buf bytes.Buffer

	if err := dotTpl.Lookup("dot").Execute(&buf, struct {
		Root  *pb.TaskInfo
		Edges []edge
	}{tasks[0], graphEdges(tasks)}); err != nil {
		return "", err
	}
	log.Printf("dotfile:\n\n%s\n", buf.String())

	cmd := exec.Command("dot", "-Tsvg")
	cmd.Stdin = &buf
	svg, err := cmd.Output()
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(svg), nil
}

// getTaskGraph recursively fetches all the tasks in a task graph,
// going up the argument tree. If it fails to fetch data from a task
// from a remote server, it will instead put a "placeholder" empty
// TaskInfo structure in its place.
func getTaskGraph(ctx context.Context, net *network, id *pb.ShardedID) []*pb.TaskInfo {
	var scanTask func(*pb.ShardedID, []*pb.TaskInfo) []*pb.TaskInfo
	scanTask = func(id *pb.ShardedID, tasks []*pb.TaskInfo) []*pb.TaskInfo {
		// Create a fake 'placeholder' task with no data.
		task := &pb.TaskInfo{
			Spec: &pb.TaskSpec{
				TaskId: id,
			},
		}
		if peer, err := net.GetQueue(ctx, id.Shard); err == nil {
			if resp, err := peer.Query(ctx, &pb.QueryRequest{TaskId: id}); err == nil {
				task = resp
			} else {
				log.Printf("error retrieving task %s: %v", id.ToString(), err)
			}
		} else {
			log.Printf("error retrieving peer for task %s: %v", id.ToString(), err)
		}
		tasks = append(tasks, task)
		for _, tid := range task.Spec.GetTaskArgIDs() {
			tasks = scanTask(tid, tasks)
		}
		return tasks
	}

	return scanTask(id, nil)
}

func (s *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/debug/task":
		s.handleTaskDebugPage(w, req)
	case "/debug/tasks":
		s.handleTaskListPage(w, req)
	default:
		http.NotFound(w, req)
	}
}
