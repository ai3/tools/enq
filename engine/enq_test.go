package engine

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"
	"google.golang.org/protobuf/types/known/timestamppb"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

func init() {
	debugMigrations = false
}

type testNet struct {
	peerMap map[string]*bufconn.Listener
	servers []*testServer
	net     *network

	workerCancel context.CancelFunc
	workerWg     sync.WaitGroup
}

func newTestNet() *testNet {
	n := &testNet{
		peerMap: make(map[string]*bufconn.Listener),
	}
	n.net = newNetwork(new(testTopology), n.dialer())
	return n
}

func (n *testNet) Close() {
	if n.workerCancel != nil {
		n.workerCancel()
	}
	n.workerWg.Wait()
	for _, lis := range n.peerMap {
		lis.Close()
	}
	for _, s := range n.servers {
		s.Close()
	}
}

func (n *testNet) dialer() Dialer {
	return func(ctx context.Context, shard string) (*grpc.ClientConn, error) {
		bn, ok := n.peerMap[shard]
		if !ok {
			return nil, errors.New("unreachable")
		}
		return grpc.DialContext(
			ctx,
			"bufnet",
			grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
				return bn.Dial()
			}),
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		)
	}
}

func (n *testNet) assertTaskState(t *testing.T, id *pb.ShardedID, state pb.TaskInfo_State, timeout time.Duration) {
	deadline := time.Now().Add(timeout)
	var resp *pb.TaskInfo
	for time.Now().Before(deadline) {
		c, err := n.net.GetQueue(context.Background(), id.Shard)
		if err != nil {
			t.Fatalf("couldn't get client for shard %s: %v", id.Shard, err)
		}
		resp, err = c.Query(context.Background(), &pb.QueryRequest{TaskId: id})
		if err != nil {
			t.Fatalf("Query(%s): %v", id.ToString(), err)
		}
		if resp.State == state {
			return
		}
		time.Sleep(500 * time.Millisecond)
	}
	t.Fatalf("task %s did not converge to state %s, still %s after %s", id.ToString(), state, resp.State, timeout)
}

// The test topology just passes along the shard ID as the network
// address, so that testNet.dialer() knows where to connect to.
type testTopology struct{}

func (*testTopology) GetShardAddr(shard string) (string, error) {
	return shard, nil
}

type testServer struct {
	dir  string
	db   *sql.DB
	svc  *Engine
	grpc *grpc.Server
}

func (n *testNet) newServer(t testing.TB, shard string) *testServer {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	db, err := OpenDB(filepath.Join(dir, "enq.db"))
	if err != nil {
		t.Fatal(err)
	}

	e := newEngine(newSQLDB(shard, db), shard, n.net)

	lis := bufconn.Listen(1024 * 1024)
	grpcsrv := grpc.NewServer(grpc.UnaryInterceptor(SqliteServerInterceptor()))
	pb.RegisterInternalServer(grpcsrv, e)
	pb.RegisterWorkerServer(grpcsrv, e)
	pb.RegisterQueueServer(grpcsrv, e)

	go func() {
		grpcsrv.Serve(lis) // nolint: errcheck
	}()

	ts := &testServer{
		dir:  dir,
		db:   db,
		svc:  e,
		grpc: grpcsrv,
	}
	n.servers = append(n.servers, ts)
	n.peerMap[shard] = lis
	return ts
}

// TODO: this does not actually stop the server :(
func (n *testNet) stopServer(shard string) {
	// Still in n.servers so it will be stopped on Close.
	n.peerMap[shard].Close()
	delete(n.peerMap, shard)
}

func (s *testServer) Close() {
	s.svc.Close()
	s.db.Close()
	os.RemoveAll(s.dir)
}

func (n *testNet) startWorkers(t testing.TB) {
	ctx, cancel := context.WithCancel(context.Background())
	n.workerCancel = cancel

	for shard := range n.peerMap {
		n.workerWg.Add(1)
		go func(shard string) {
			runTestWorker(ctx, t, n, shard, map[string]taskFunc{
				"sum":  sumFunc,
				"neg":  negFunc,
				"fail": failFunc,
			})
			n.workerWg.Done()
		}(shard)
	}
}

type taskFunc func(*pb.Task) ([]byte, error)

func runTestWorker(ctx context.Context, t testing.TB, n *testNet, shard string, funcs map[string]taskFunc) {
	peer, err := n.net.getWorker(ctx, shard)
	if err != nil {
		t.Fatalf("worker %s: client setup error: %v", shard, err)
	}

	for {
		cctx, cancel := context.WithTimeout(ctx, 1*time.Second)
		resp, err := peer.Poll(cctx, &pb.PollRequest{
			WorkerId:      fmt.Sprintf("worker-%s", shard),
			WorkerBaseUrl: fmt.Sprintf("http://worker-%s:1234", shard),
		})
		cancel()
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.Canceled:
				return
			case codes.Unavailable:
				time.Sleep(100 * time.Millisecond)
				continue
			}
		}
		if err != nil {
			t.Errorf("worker %s: Poll() error: %v", shard, err)
			return
		}
		if resp.Spec == nil {
			// Nothing to do, wait.
			time.Sleep(300 * time.Millisecond)
			continue
		}

		log.Printf("worker %s: poll -> %s", shard, resp)

		tst := &pb.TaskStateTransition{
			TaskId: resp.Spec.TaskId,
		}

		f, ok := funcs[resp.Spec.Method]
		if !ok {
			log.Printf("worker %s: method not found: %s", shard, resp.Spec.Method)
			tst.ResultStatus = pb.TaskStateTransition_FAILURE
		} else {
			result, err := f(resp)
			if err != nil {
				tst.ResultStatus = pb.TaskStateTransition_FAILURE
			} else {
				tst.ResultStatus = pb.TaskStateTransition_SUCCESS
				tst.ResultData = result
			}
		}

		log.Printf("worker %s: done -> %s", shard, tst)
		cctx, cancel = context.WithTimeout(ctx, 1*time.Second)
		_, err = peer.Done(cctx, tst)
		cancel()
		if err != nil {
			if e, ok := status.FromError(err); ok && e.Code() == codes.Canceled {
				return
			}
			t.Errorf("worker %s: Done() error: %v", shard, err)
		}
	}
}

func sumFunc(task *pb.Task) ([]byte, error) {
	if len(task.Args) != 2 {
		return nil, errors.New("bad number of args")
	}
	var arg1, arg2 int
	if err := json.Unmarshal(task.Args[0], &arg1); err != nil {
		return nil, err
	}
	if err := json.Unmarshal(task.Args[1], &arg2); err != nil {
		return nil, err
	}
	sum := arg1 + arg2
	log.Printf("sum(%d, %d) -> %d", arg1, arg2, sum)
	return json.Marshal(sum)
}

func negFunc(task *pb.Task) ([]byte, error) {
	if len(task.Args) != 1 {
		return nil, errors.New("bad number of args")
	}
	var arg int
	if err := json.Unmarshal(task.Args[0], &arg); err != nil {
		return nil, err
	}
	neg := -arg
	log.Printf("neg(%d) -> %d", arg, neg)
	return json.Marshal(neg)
}

func failFunc(task *pb.Task) ([]byte, error) {
	log.Printf("fail()")
	return nil, errors.New("oh no I failed")
}

func TestQueue(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "aaa",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("40"),
						},
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("2"),
						},
					},
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "5",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "2",
								Id:    "aaa",
							},
						},
					},
				},
			},
		},
	)
	if err != nil {
		t.Fatal(err)
	}

	if resp.RootTaskId.Shard != "5" {
		t.Fatalf("bad response root task shard: expected 5, got %s", resp.RootTaskId.Shard)
	}
	if resp.RootTaskId.Id == "bbb" {
		t.Fatal("bad response root task ID (not rewritten)")
	}

	n.assertTaskState(t, resp.RootTaskId, pb.TaskInfo_SUCCESS, 3*time.Second)
}

func TestQueue_SameShard(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	// Same as TestQueue, but tasks run on the same shard to highlight
	// transaction deadlocking issues.
	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "aaa",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("40"),
						},
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("2"),
						},
					},
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "2",
								Id:    "aaa",
							},
						},
					},
				},
			},
		},
	)
	if err != nil {
		t.Fatal(err)
	}

	n.assertTaskState(t, resp.RootTaskId, pb.TaskInfo_SUCCESS, 3*time.Second)
}

func TestQueue_Fail(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	deadline := time.Now().AddDate(1, 0, 0)

	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "aaa",
					},
					Method:    "fail",
					NotBefore: timestamppb.New(time.Now()),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "5",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "2",
								Id:    "aaa",
							},
						},
					},
					NotBefore: timestamppb.New(time.Now()),
					Deadline:  timestamppb.New(deadline),
				},
			},
		},
	)

	if err != nil {
		t.Fatal(err)
	}

	n.assertTaskState(t, resp.RootTaskId, pb.TaskInfo_FAILED, 3*time.Second)
}

func TestQueue_Cancel(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	deadline := time.Now().AddDate(1, 0, 0)

	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "aaa",
					},
					Method:    "fail",
					NotBefore: timestamppb.New(time.Now()),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "3",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("42"),
						},
					},
					// Schedule this one sometime in the
					// future so it has a chance to be
					// canceled.
					NotBefore: timestamppb.New(time.Now().AddDate(0, 0, 1)),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "4",
						Id:    "ccc",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("19"),
						},
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "3",
								Id:    "bbb",
							},
						},
					},
					NotBefore: timestamppb.New(time.Now()),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "5",
						Id:    "ddd",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "4",
								Id:    "ccc",
							},
						},
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "2",
								Id:    "aaa",
							},
						},
					},
					NotBefore: timestamppb.New(time.Now()),
					Deadline:  timestamppb.New(deadline),
				},
			},
		},
	)

	if err != nil {
		t.Fatal(err)
	}

	n.assertTaskState(t, resp.RootTaskId, pb.TaskInfo_FAILED, 3*time.Second)
}

// TODO: This does not yet test *failure*!
func TestQueue_IndependentFailure(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	// Run tasks one second in the future, which should be enough time
	// for the Submit() call to complete, allowing us to stop one of the
	// nodes.
	deadline := time.Now().AddDate(1, 0, 0)
	when := time.Now().Add(1 * time.Second)

	okShard := "2"
	badShard := "5"
	subTaskShard := []string{okShard, badShard}
	rootIDs := make([]*pb.ShardedID, 2)

	// Submit two independent short task trees. One of them has a task
	// on a different shard, which will become unreachable. Success of
	// its upstream task will generate a TaskStateTransition that can't
	// be applied.
	c, _ := n.net.GetQueue(context.Background(), "1")
	for i := 0; i < 2; i++ {
		resp, err := c.Submit(
			context.Background(),
			&pb.SubmitRequest{
				Tasks: []*pb.TaskSpec{
					&pb.TaskSpec{
						TaskId: &pb.ShardedID{
							Shard: okShard,
							Id:    "aaa",
						},
						Method: "sum",
						Args: []*pb.ArgumentSpec{
							&pb.ArgumentSpec{
								Type:      pb.ArgumentSpec_VALUE,
								ValueJson: []byte("40"),
							},
							&pb.ArgumentSpec{
								Type:      pb.ArgumentSpec_VALUE,
								ValueJson: []byte("2"),
							},
						},
						NotBefore: timestamppb.New(when),
						Deadline:  timestamppb.New(deadline),
					},
					&pb.TaskSpec{
						TaskId: &pb.ShardedID{
							Shard: subTaskShard[i],
							Id:    "bbb",
						},
						Method: "neg",
						Args: []*pb.ArgumentSpec{
							&pb.ArgumentSpec{
								Type: pb.ArgumentSpec_TASKREF,
								TaskId: &pb.ShardedID{
									Shard: okShard,
									Id:    "aaa",
								},
							},
						},
						NotBefore: timestamppb.New(when),
						Deadline:  timestamppb.New(deadline),
					},
				},
			})
		if err != nil {
			t.Fatal(err)
		}
		rootIDs[i] = resp.RootTaskId
	}

	n.stopServer(badShard)

	n.assertTaskState(t, rootIDs[0], pb.TaskInfo_SUCCESS, 3*time.Second)
}

func TestQuery(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "5",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("42"),
						},
					},
				},
			},
		},
	)
	if err != nil {
		t.Fatal(err)
	}

	// Call Query() on a different shard.
	_, err = c.Query(context.Background(), &pb.QueryRequest{TaskId: resp.RootTaskId})
	if err != nil {
		t.Fatalf("Query() on different shard failed: %v", err)
	}
}

func TestLog(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	n.newServer(t, "1")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	taskID, _ := pb.ParseShardedID("1/abcdef")
	logData := "hello world!"
	logParts := []string{"hello ", "wor", "ld!"}

	w, _ := n.net.getWorker(ctx, "1")

	for _, data := range logParts {
		_, err := w.Log(ctx, &pb.LogRequest{
			TaskId: taskID,
			Data:   []byte(data),
		})
		if err != nil {
			t.Fatalf("Log() error: %v", err)
		}
	}

	c, _ := n.net.GetQueue(ctx, "1")
	stream, err := c.GetLogs(ctx, &pb.GetLogsRequest{TaskId: taskID})
	if err != nil {
		t.Fatalf("GetLogs() error: %v", err)
	}
	var result []byte
	for {
		chunk, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf("stream Recv() error: %v", err)
		}
		result = append(result, chunk.Data...)
	}

	if s := string(result); s != logData {
		t.Fatalf("GetLogs() response is '%s', expected '%s'", s, logData)
	}
}
