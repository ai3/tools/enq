package engine

import (
	"context"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	numRunningStatsCounter = prometheus.NewDesc(
		"enq_queue_running",
		"Number of tasks in the 'running' queue.",
		nil, nil,
	)
	numSchedulableStatsCounter = prometheus.NewDesc(
		"enq_queue_schedulable",
		"Number of tasks in the 'schedulable' queue.",
		nil, nil,
	)
	numPendingStatsCounter = prometheus.NewDesc(
		"enq_queue_pending",
		"Number of tasks in the 'pending' queue.",
		nil, nil,
	)
	numDoneStatsCounter = prometheus.NewDesc(
		"enq_queue_done",
		"Number of completed tasks.",
		nil, nil,
	)
)

type engineMetricCollector struct {
	db database
}

func (c *engineMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(c, ch)
}

func (c *engineMetricCollector) Collect(ch chan<- prometheus.Metric) {
	var stats *stats
	if err := c.db.WithTx(context.Background(), func(tx databaseTx) (err error) {
		stats, err = tx.GetStats()
		return
	}); err != nil {
		return
	}

	ch <- prometheus.MustNewConstMetric(
		numRunningStatsCounter,
		prometheus.GaugeValue,
		float64(stats.NumRunning),
	)
	ch <- prometheus.MustNewConstMetric(
		numSchedulableStatsCounter,
		prometheus.GaugeValue,
		float64(stats.NumSchedulable),
	)
	ch <- prometheus.MustNewConstMetric(
		numPendingStatsCounter,
		prometheus.GaugeValue,
		float64(stats.NumPending),
	)
	ch <- prometheus.MustNewConstMetric(
		numDoneStatsCounter,
		prometheus.CounterValue,
		float64(stats.NumDone),
	)
}
