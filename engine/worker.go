package engine

import (
	"context"
	"errors"
	"fmt"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"google.golang.org/protobuf/types/known/emptypb"
)

// Done reports a task termination from a worker. In order to free the
// worker from having to maintain any persistent state (consider the
// case where handling the task state transition involves making
// remote RPCs to an unresponsive shard), we do not process the
// transition right away but instead add it to a queue.
func (s *Engine) Done(ctx context.Context, ts *pb.TaskStateTransition) (*emptypb.Empty, error) {
	// Ensure request is well-formed.
	if ts.TaskId == nil {
		return nil, errors.New("empty TaskId")
	}
	if ts.TaskId.Id == "" {
		return nil, errors.New("empty TaskId.Id")
	}

	var tid rowID
	err := s.db.WithTx(ctx, func(tx databaseTx) (err error) {
		tid, err = tx.PushTaskStateTransition(ts)
		return
	})
	if err != nil {
		return nil, err
	}

	// Try to act on the state transition right away, but only if
	// the queue is not full.
	select {
	case s.notifyCh <- tid:
	default:
	}

	return emptyResponse, nil
}

// Keepalive renews the lease for a running task on a worker.
func (s *Engine) Keepalive(ctx context.Context, req *pb.KeepaliveRequest) (*emptypb.Empty, error) {
	err := s.db.WithTx(ctx, func(tx databaseTx) error {
		return tx.RenewLease(req.TaskId)
	})
	return emptyResponse, err
}

// Poll returns a task to be processed by a worker, if any are
// available. An empty result is indicated not by an error (which
// would confuse the GRPC statistics) but by an empty message with no
// fields set.
//
// When a task is ready to be run, the results of all its upstream
// dependencies will already be present on this shard.
func (s *Engine) Poll(ctx context.Context, req *pb.PollRequest) (*pb.Task, error) {
	var task pb.Task
	err := s.db.WithTx(ctx, func(tx databaseTx) error {
		taskID, err := tx.NextLease(req.WorkerId, req.WorkerBaseUrl)
		if err != nil {
			return err
		}
		if taskID == nil {
			// Nothing in the queue.
			return nil
		}

		spec, err := tx.GetTaskSpec(taskID)
		if err != nil {
			return fmt.Errorf("Poll(GetTaskSpec): %w", err)
		}
		task.Spec = spec

		// Build the args list. All task results should be
		// local at this point.
		args := make([][]byte, 0, len(spec.Args))
		for _, arg := range spec.Args {
			switch arg.Type {
			case pb.ArgumentSpec_VALUE:
				args = append(args, arg.ValueJson)
			case pb.ArgumentSpec_TASKREF:
				result, err := tx.GetTaskResultByTaskID(arg.TaskId)
				if err != nil {
					return fmt.Errorf("Poll(GetTaskResult(%s)): %w", arg.TaskId.ToString(), err)
				}
				args = append(args, result)
			}
		}
		task.Args = args

		return nil
	})
	return &task, err
}

// Log stores a log fragment in the db.
func (s *Engine) Log(ctx context.Context, req *pb.LogRequest) (*emptypb.Empty, error) {
	err := s.db.WithTx(ctx, func(tx databaseTx) error {
		return tx.AddLog(req.TaskId, req.Data)
	})
	return emptyResponse, err
}
