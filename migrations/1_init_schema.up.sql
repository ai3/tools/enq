
CREATE TABLE tasks (
  id TEXT PRIMARY KEY,
  method TEXT NOT NULL,
  -- max_failures INTEGER,
  created_at DATETIME,
  not_before DATETIME,
  downstream_task_id TEXT,
  result_id TEXT
);

CREATE TABLE task_args (
  task_id TEXT,
  arg_type INTEGER,
  value_json BLOB,
  ref_task_id TEXT,
  has_result INTEGER DEFAULT 0,
  FOREIGN KEY(task_id) REFERENCES tasks(id)
);

CREATE INDEX idx_task_args_task_id ON task_args(task_id);
CREATE INDEX idx_task_args_ref_task_id ON task_args(ref_task_id);

-- The tables for various task states.
CREATE TABLE scheduled (
  task_id TEXT PRIMARY KEY,
  schedule_time DATETIME NOT NULL,
  FOREIGN KEY(task_id) REFERENCES tasks(id)
);

CREATE TABLE running (
  task_id TEXT PRIMARY KEY,
  started_at DATETIME NOT NULL,
  pinged_at DATETIME,
  url TEXT,
  FOREIGN KEY(task_id) REFERENCES tasks(id)
);

CREATE TABLE pending (
  task_id TEXT PRIMARY KEY,
  FOREIGN KEY(task_id) REFERENCES tasks(id)
);

CREATE TABLE done (
  task_id TEXT PRIMARY KEY,
  state INTEGER,
  completed_at DATETIME,
  FOREIGN KEY(task_id) REFERENCES tasks(id)
);

-- A queue of task state transitions that need to be processed.
CREATE TABLE pending_transitions (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  task_id TEXT,
  result_status INTEGER,
  result_data BLOB,
  pending_since DATETIME NOT NULL
);

-- Tables for associated data.
CREATE TABLE results (
  result_id TEXT PRIMARY KEY,
  task_id TEXT,
  value BLOB
);

CREATE INDEX idx_results_task_id ON results(task_id);

CREATE TABLE logs (
  task_id TEXT,
  data BLOB,
  timestamp DATETIME NOT NULL
);

CREATE INDEX idx_logs_task_id ON logs(task_id);

CREATE TABLE execution_log (
  task_id INTEGER NOT NULL,
  host TEXT,
  started_at DATETIME,
  completed_at DATETIME,
  error_message TEXT,
  is_error INTEGER,
  is_retriable INTEGER
);
