package worker

import (
	"context"
	"html/template"
	"log"
	"net/http"
	"sync"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

var (
	debugTplSrc = `<!doctype html>
<html lang="en">
 <head>
  <title>ENQ Worker</title>
 </head>
 <body>
  <h1>Enq - Running tasks</h1>
  <table>
   <thead>
    <tr>
     <th>ID</th>
     <th>Method</th>
     <th>Start Time</th>
    </tr>
   </thead>
   <tbody>
{{range $idx, $state := .Tasks}}
    <tr>
     <td>{{$state.Task.Spec.TaskId}}</td>
     <td>{{$state.Task.Spec.Method}}</td>
     <td>{{$state.StartedAt}}</td>
    </tr>
{{end}}
   </tbody>
  </table>
 </body>
</html>`

	debugTpl = template.Must(template.New("").Parse(debugTplSrc))
)

type taskState struct {
	Task      *pb.Task
	StartedAt time.Time
}

var (
	currentTasksMx sync.Mutex
	currentTasks   map[string]*taskState
)

func init() {
	currentTasks = make(map[string]*taskState)
}

func pushCurrentTask(task *pb.Task) {
	currentTasksMx.Lock()
	currentTasks[task.Spec.TaskId.String()] = &taskState{
		Task:      task,
		StartedAt: time.Now(),
	}
	currentTasksMx.Unlock()
}

func popCurrentTask(task *pb.Task) {
	currentTasksMx.Lock()
	delete(currentTasks, task.Spec.TaskId.String())
	currentTasksMx.Unlock()
}

func getCurrentTasks() []*taskState {
	currentTasksMx.Lock()
	defer currentTasksMx.Unlock()
	out := make([]*taskState, 0, len(currentTasks))
	for _, state := range currentTasks {
		out = append(out, state)
	}
	return out
}

type debugTaskHandler struct {
	taskHandler
}

func newDebugTaskHandler(h taskHandler) taskHandler {
	return &debugTaskHandler{taskHandler: h}
}

func (h *debugTaskHandler) HandleTask(ctx context.Context, task *pb.Task, l *log.Logger) ([]byte, error) {
	pushCurrentTask(task)
	defer popCurrentTask(task)
	return h.taskHandler.HandleTask(ctx, task, l)
}

func (m *Manager) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	debugTpl.Execute(w, struct {
		Tasks []*taskState
	}{Tasks: getCurrentTasks()})
}
