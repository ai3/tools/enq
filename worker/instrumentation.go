package worker

import (
	"context"
	"log"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"github.com/prometheus/client_golang/prometheus"
)

var taskCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "enq_task_executions_total",
		Help: "Total number of task executions.",
	},
	[]string{"method", "status"},
)

func init() {
	prometheus.MustRegister(taskCounter)
}

type instrumentedTaskHandler struct {
	taskHandler
}

func newInstrumentedTaskHandler(h taskHandler) taskHandler {
	return &instrumentedTaskHandler{taskHandler: h}
}

func (h *instrumentedTaskHandler) HandleTask(ctx context.Context, task *pb.Task, l *log.Logger) ([]byte, error) {
	resp, err := h.taskHandler.HandleTask(ctx, task, l)
	taskCounter.WithLabelValues(task.Spec.Method, statusFromErr(err)).Inc()
	return resp, err
}

func statusFromErr(err error) string {
	switch {
	case err == nil:
		return "ok"
	case isTempError(err):
		return "tempfail"
	default:
		return "error"
	}
}
