package worker

import (
	"context"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

type logWriter struct {
	client  pb.WorkerClient
	taskID  *pb.ShardedID
	timeout time.Duration
}

func newLogWriter(client pb.WorkerClient, taskID *pb.ShardedID) *logWriter {
	return &logWriter{
		client:  client,
		taskID:  taskID,
		timeout: 5 * time.Second,
	}
}

func (w *logWriter) Write(data []byte) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), w.timeout)
	defer cancel()
	_, err := w.client.Log(ctx, &pb.LogRequest{
		TaskId: w.taskID,
		Data:   data,
	})
	if err != nil {
		return 0, err
	}
	return len(data), nil
}
