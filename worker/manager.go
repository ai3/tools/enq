package worker

import (
	"bufio"
	"context"
	"log"
	"sync"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// This function wrapper will just call f() while simultaneously
// making Keepalive RPC periodically for as long as it runs.
func withKeepalive(ctx context.Context, client pb.WorkerClient, taskID *pb.ShardedID, f func() error, keepaliveInterval time.Duration) error {
	subCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	go func(ctx context.Context) {
		keepaliveTimeout := keepaliveInterval / 3
		ticker := time.NewTicker(keepaliveInterval)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				cctx, cancel := context.WithTimeout(ctx, keepaliveTimeout)
				_, err := client.Keepalive(cctx, &pb.KeepaliveRequest{
					TaskId: taskID,
				})
				cancel()
				if err != nil {
					log.Printf("keepalive error for task %x: %v", taskID.ToString(), err)
				}
			case <-ctx.Done():
				return
			}
		}
	}(subCtx)

	return f()
}

// Manager controls a worker pool connected to a queue.
type Manager struct {
	id  string
	url string

	pollTimeout       time.Duration
	pollInterval      time.Duration
	keepaliveInterval time.Duration

	handler taskHandler
	conn    *grpc.ClientConn
	wg      sync.WaitGroup
	cancel  context.CancelFunc
}

// NewManager starts a worker pool of size numWorkers connected to the given
// (presumably local) queue, running scripts off the handlersDir.
func NewManager(conn *grpc.ClientConn, id, baseURL, handlersDir string, numWorkers int) *Manager {
	m := &Manager{
		id:   id,
		url:  baseURL,
		conn: conn,
		handler: newInstrumentedTaskHandler(
			newDebugTaskHandler(
				&procTaskHandler{dir: handlersDir})),
		pollTimeout:       30 * time.Second,
		pollInterval:      1 * time.Second,
		keepaliveInterval: 10 * time.Second,
	}

	// Run workers with a context that can be canceled.
	ctx, cancel := context.WithCancel(context.Background())
	m.cancel = cancel
	for i := 0; i < numWorkers; i++ {
		m.wg.Add(1)
		go m.runWorker(ctx)
	}

	return m
}

func (m *Manager) Stop() {
	m.cancel()
}

func (m *Manager) Wait() {
	m.wg.Wait()
}

func (m *Manager) runWorker(ctx context.Context) {
	defer m.wg.Done()

	c := pb.NewWorkerClient(m.conn)

	for {
		cctx, cancel := context.WithTimeout(ctx, m.pollTimeout)
		task, err := c.Poll(cctx, &pb.PollRequest{
			WorkerId:      m.id,
			WorkerBaseUrl: m.url,
		})
		cancel()
		if e, ok := status.FromError(err); ok && e.Code() == codes.Canceled {
			return
		}
		if err != nil {
			log.Printf("worker %s: Poll() error: %v", m.id, err)
			time.Sleep(m.pollInterval * 2)
			continue
		}
		if task.Spec == nil {
			// Nothing to do, wait.
			time.Sleep(m.pollInterval)
			continue
		}

		log.Printf("worker %s: poll -> %s", m.id, task)

		// Create a remote Logger and run the handler while sending keepalives.
		var result []byte
		logger := log.New(bufio.NewWriter(newLogWriter(c, task.Spec.TaskId)), "", log.LstdFlags)
		err = withKeepalive(ctx, c, task.Spec.TaskId, func() (err error) {
			result, err = m.handler.HandleTask(ctx, task, logger)
			return
		}, m.keepaliveInterval)

		tst := &pb.TaskStateTransition{
			TaskId:     task.Spec.TaskId,
			ResultData: result,
		}
		if err != nil {
			if isTempError(err) {
				tst.ResultStatus = pb.TaskStateTransition_TEMPORARY_FAILURE
			} else {
				tst.ResultStatus = pb.TaskStateTransition_FAILURE
			}
		} else {
			tst.ResultStatus = pb.TaskStateTransition_SUCCESS
		}

		log.Printf("worker %s: done -> %s", m.id, tst)
		cctx, cancel = context.WithTimeout(ctx, m.pollTimeout)
		_, err = c.Done(cctx, tst)
		cancel()
		if err != nil {
			log.Printf("worker %s: Done() error: %v", m.id, err)
		}
	}

}
